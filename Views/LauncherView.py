__author__ = 'ryoh'

from Views.IView import IView

from tkinter import *
from tkinter import ttk


class LauncherView(IView):

    def __init__(self):
        super().__init__()
        self._root = Toplevel()
        self._root.title("Godot Launcher")
        self._root.resizable(False, False)
        self._root.protocol("WM_DELETE_WINDOW", exit)

        self._picture = ttk.Label(self._root)
        self._picture.pack(padx=70)
        ttk.logo = PhotoImage(file="Resource/logo.gif")
        self._picture.config(image=ttk.logo)

        self._versions_frame = ttk.LabelFrame(self._root, text='Versions:')
        self._versions_frame.pack(fill=X)

        self._version_selected = StringVar()
        self._versions_combobox = ttk.Combobox(self._versions_frame, textvariable=self._version_selected)
        self._versions_combobox.pack(fill=X)

        self._buttons_frame = ttk.Frame(self._root)
        self._buttons_frame.pack()

        self._buttons = dict()

        self._buttons['manager'] = ttk.Button(self._buttons_frame, text='manager')
        self._buttons['manager'].pack(side=RIGHT, padx=5)

        self._buttons['launch'] = ttk.Button(self._buttons_frame, text='Launch')
        self._buttons['launch'].pack(side=LEFT, padx=5)

    def set_button_action(self, **kwargs):
        arg_set = False

        if 'launch' in kwargs:
            self._buttons['launch'].config(command=kwargs['launch'])
            arg_set = True

        if 'manager' in kwargs:
            self._buttons['manager'].config(command=kwargs['manager'])
            arg_set = True

        if not arg_set:
            raise KeyError

    def destroy(self):
        self._root.destroy()

    def mainloop(self):
        self._root.mainloop()

    def get_version(self):
        return self._version_selected

    def set_versions(self, versions):
        self._versions_combobox.config(values=versions)


############
def main():
    w = LauncherView()
    w.set_button_action(launch=lambda: print('launch'), manager=lambda: print('manager'))
    w.mainloop()

if __name__ == "__main__":
    main()