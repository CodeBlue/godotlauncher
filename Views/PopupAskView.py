__author__ = 'ryoh'

from tkinter import *
from tkinter import ttk

from Views.IView import IView


class PopupAskView(IView):

    def __init__(self, message):
        super().__init__()
        self._root = Toplevel()
        self._root.title("Godot Launcher")
        self._root.protocol("WM_DELETE_WINDOW", exit)

        self._message = ttk.Label(self._root, text=message)
        self._message.pack()

        self._buttons = {}
        self._button_frame = ttk.Frame(self._root)
        self._button_frame.pack()
        self._buttons['yes'] = ttk.Button(self._button_frame, text="Yes")
        self._buttons['yes'].pack(side=LEFT)

        self._buttons['no'] = ttk.Button(self._button_frame, text="No")
        self._buttons['no'].pack(side=LEFT)

    def set_button_action(self, **kwargs):
        arg_set = False

        if 'yes' in kwargs:
            self._buttons['yes'].config(command=kwargs['yes'])

            arg_set = True
        if 'no' in kwargs:
            self._buttons['no'].config(command=kwargs['no'])
            arg_set = True

        if not arg_set:
            raise KeyError

    def destroy(self):
        self._root.destroy()

    def mainloop(self):
        self._root.mainloop()


def main():
    w = PopupAskView("Yes or no?")
    w.set_button_action(yes=lambda: print('yes'), no=lambda: print('no'))

if __name__ == "__main__":
    main()