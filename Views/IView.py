__author__ = 'ryoh'


class IView:

    def __init__(self):
        pass

    def set_button_action(self):
        raise NotImplementedError

    def mainloop(self):
        raise NotImplementedError

    def destroy(self):
        raise NotImplementedError