__author__ = 'ryoh'

# TODO use sqlite

# import sqlite3
import os.path
import datetime

DB_NAME = 'versions.db'

_ENTRY_DELIM = ';'
_LINE_ENDING = '\n'


class DataBaseEntry:
    url = ''
    file_name = ''
    release_date = ''
    key = ''

    INSTALLED = 'installed'
    UNINSTALLED = 'uninstalled'
    status = UNINSTALLED

    def __init__(self, url, file_name, release_date, status):
        self.url = url
        self.key = url
        self.file_name = file_name
        year = int(release_date.split('-')[0])
        month = int(release_date.split('-')[1])
        day = int(release_date.split('-')[2])
        self.release_date = datetime.date(year, month, day)
        self.status = status

    def get_release_date(self):
        return str(self.release_date)

    def __str__(self):
        return self.url + ";" + self.file_name + ";" + self.release_date + ";" + str(self.status)

    def __cmp__(self, other):
        return self.release_date.__cmp__(other)


class Database:
    _entry_list = {}
    _database = None
    _database_path = ''

    def __init__(self, path):
        self._database_path = path

    def save(self):
        with open(self._database_path + '/' + DB_NAME, 'w') as database:
            for entry in self._entry_list.keys():
                database.write(str(self._entry_list[entry]) + _LINE_ENDING)

    def load(self):
        # check if file exists
        if not os.path.isfile(DB_NAME):
            return

        # if it does
        with open(DB_NAME, 'r') as database:
            while True:
                line = database.readline().strip()
                if not line:
                    break
                entry = self._parse_line(line)
                self._entry_list[entry.key] = entry

    @staticmethod
    def _parse_line(line):
        line = line.split(_ENTRY_DELIM)
        db_entry = DataBaseEntry(line[0], line[1], line[2], line[3])
        return db_entry

    def add_entry(self, db_entry):
        if db_entry.key not in self._entry_list:
            self._entry_list[db_entry.key] = db_entry

    def remove_entry(self, db_entry):
        self._entry_list.pop(db_entry.key)

    def update_entry(self, key, db_entry):
        if key not in self._entry_list:
            assert isinstance(db_entry, DataBaseEntry)
            self._entry_list[key] = db_entry

    def get_versions(self):
        return self._entry_list.values()

    def remove_uninstalled_entries(self):
        """ This method relies on the idea that an entry wouldn't have been
            in the database if it didn't exist at some point. it should keep people
            from losing versions based on server errors"""
        temp = {}
        for entry in self._entry_list.keys():
            if self._entry_list[entry].status == DataBaseEntry.INSTALLED:
                temp[entry] = self._entry_list[entry]
        self._entry_list = temp