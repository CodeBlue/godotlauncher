__author__ = 'ryoh'

from Models.Core.Database import DataBaseEntry

class DataCollector:
    _db_entries = []

    def refreshCatalog(self):
        """ fetch another list from the internet"""
        self._db_entries = []
        self._db_entries.append(DataBaseEntry("https://godot.blob.core.windows.net/release/2014-12-15/godot_win64-1.0stable.exe", "godot_win64-1.0stable.exe", "2014-12-15", DataBaseEntry.UNINSTALLED))
        self._db_entries.append(DataBaseEntry("https://godot.blob.core.windows.net/devel/2014-11-27/godot_win64-1.0devel.exe", "godot_win64-1.0devel.exe", "2014-11-27", DataBaseEntry.UNINSTALLED))
        self._db_entries.append(DataBaseEntry("https://godot.blob.core.windows.net/release/2014-12-08/godot_win64-1.0rc2.exe", "godot_win64-1.0rc2.exe", "2014-12-08", DataBaseEntry.UNINSTALLED))

    def getEntries(self):
        """ return a list of data base entries"""
        return self._db_entries