__author__ = 'ryoh'

import platform
import os


class OSDirectory:

    _NAME = ".godot_launcher"

    def __init__(self):
        self._platform_directory = self.get_platform_directory()

    def make_directory(self):
        if not os.path.isdir(self._platform_directory):
            os.mkdir(self._platform_directory)

    def remove_directory(self):
        if os.path.isdir(self._platform_directory):
            os.rmdir(self._platform_directory)

    def directory_exists(self):
        return os.path.isdir(self._platform_directory)

    def get_platform_directory(self):
        if platform.system() == "Windows":
            #return r'C:\Users\ryoh\Desktop\godot launcher'
            return 'C:/Users/ryoh/AppData/Roaming/' + self._NAME

        if platform.system() == "Linux":
            return "~/.godot" + self._NAME

def getInstallPath():
    if platform.system() == "Windows":
        #return r'C:\Users\ryoh\Desktop\godot launcher'
        return r'C:\Users\ryoh\AppData\Roaming\godotlauncher'