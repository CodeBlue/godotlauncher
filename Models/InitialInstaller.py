__author__ = 'ryoh'

import os

from Models.GodotVersionManager import GodotVersionManager
from Models.Core import OSDirectory


class InitialInstaller:

    _version_manager = None
    _most_recent = None
    _install_directory = None

    def __init__(self):
        self._install_directory = OSDirectory.getInstallPath()
        self._version_manager = GodotVersionManager(self._install_directory)

    def configure_system(self):
        os.mkdir(self._install_directory)

    def download_recent_version(self):
        self._version_manager.refresh()
        self._most_recent = max(self._version_manager.getVersions())
        return self._version_manager.download(self._most_recent)

    def install_recent_version(self):
        self._version_manager.install(self._most_recent)
        self._version_manager.saveDatabase()