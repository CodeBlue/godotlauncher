__author__ = 'ryoh'

from Models.Core.Database import DataBaseEntry
from Models.Core.Database import Database
from Models.Core.Downloader import Downloader
from Models.Core.DataCollector import DataCollector


class GodotVersionManager:
    _database = None
    _data_collector = None
    _database_directory = None

    def __init__(self, database_directory):
        self._database = Database(database_directory)
        self._database_directory = database_directory
        self._data_collector = DataCollector()
    
    def refresh(self):
        self._database.remove_uninstalled_entries()
        self._data_collector.refreshCatalog()
        for entry in self._data_collector.getEntries():
            self._database.add_entry(entry)

    def download(self, db_entry):
        downloader = Downloader(db_entry.url, self._database_directory)
        downloader.start()
        return downloader.get_download()

    def install(self, db_entry):
        db_entry.status = DataBaseEntry.INSTALLED
        self._database.update_entry(db_entry.url, db_entry)

    def remove(self, db_entry):
        self._database.remove_entry(db_entry)

    def getVersions(self):
        return self._database.get_versions()

    def saveDatabase(self):
        self._database.save()

    def loadDatabase(self):
        self._database.load()

    def latest_version_installed(self):
        versions = self.getVersions()

        if not versions:
            return False

        most_recent_version = max(versions)

        if most_recent_version.status == DataBaseEntry.INSTALLED:
            return True
        else:
            return False