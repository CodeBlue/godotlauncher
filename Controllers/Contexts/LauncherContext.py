__author__ = 'ryoh'

from Controllers.Contexts.IContext import IContext
from Controllers.Contexts.VersionManagerContext import VersionManagerContext

from Views.LauncherView import LauncherView

from Models.GodotVersionManager import GodotVersionManager
from Models.Core.OSDirectory import getInstallPath
from Models.Core.ProgramLauncher import launch_program
from Models.Core.Database import DataBaseEntry


class LauncherContext(IContext):

    _next_context = None

    def __init__(self):
        super().__init__()

        self._window = LauncherView()
        self._window.set_button_action(launch=self._launch)
        self._window.set_button_action(manager=self._manager)

        self._version_manager = GodotVersionManager(getInstallPath())
        self._version_manager.refresh()

        file_names = [x.file_name for x in self._version_manager.getVersions() if x.status == DataBaseEntry.INSTALLED]
        self._window.set_versions(file_names)

    def _launch(self):
        version = self._window.get_version().get()
        if version:
            launch_program(getInstallPath() + '/' + version)
            self._window.destroy()
            exit()
        else:
            # TODO tell user that the reason nothing is happening is because nothing is selected
            pass

    def _manager(self):
        self._next_context = VersionManagerContext()
        self._window.destroy()

    def run(self):
        self._window.mainloop()

        if self._next_context is None:
            return self._next_context
