__author__ = 'jmann21'

from Controllers.Contexts.IContext import IContext
from Controllers.Contexts.CheckLauncerVersionContext import CheckLauncherVersionContext
from Controllers.Contexts.InstallContext import InstallContext

from Models.Core.OSDirectory import OSDirectory


class InitContext(IContext):
    def __init__(self):
        super().__init__()
        self.os_dir = OSDirectory()

    def run(self):
        if not self.os_dir.directory_exists():
            self.os_dir.make_directory()

        return CheckLauncherVersionContext()