__author__ = 'jmann21'

from Controllers.Contexts.IContext import IContext

from Controllers.Contexts.AskToUpdateLauncherContext import AskToUpdateLauncherContext
from Controllers.Contexts.LauncherContext import LauncherContext

from Models.LauncherVersionManager import LauncherVersionManager


class CheckLauncherVersionContext(IContext):

    def __init__(self):
        super().__init__()
        self._launcher_version_manager = LauncherVersionManager()

    def run(self):

        if self._launcher_version_manager.latest_version_installed():
            return LauncherContext()
        else:
            return AskToUpdateLauncherContext()
