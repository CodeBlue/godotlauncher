__author__ = 'ryoh'

from Controllers.Contexts.IContext import IContext

from Controllers.Contexts.LauncherContext import LauncherContext

from Views.PopupAskView import PopupAskView


class AskToUpdateLauncherContext(IContext):

    _next_context = None

    def __init__(self):
        super().__init__()

        self._window = PopupAskView("Would you like to update the Launcher?")
        self._window.set_button_action(no=self._no)
        self._window.set_button_action(yes=self._yes)

    def _no(self):
        self._next_context = LauncherContext()
        self._window.destroy()

    def _yes(self):
        self._next_context = None
        self._window.destroy()

    def run(self):
        self._window.mainloop()
        return self._next_context