__author__ = 'ryoh'

from Controllers.Contexts.InitContext import InitContext


class ContextManager:

    def __init__(self):
        self._starting_context = InitContext()

    def start(self):
        context = self._starting_context

        while True:
            context = context.run()

            if context is None:
                break
